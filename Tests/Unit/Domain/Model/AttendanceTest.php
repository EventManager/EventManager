<?php
namespace KayStrobach\EventManager\Tests\Unit\Domain\Model;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

/**
 * Testcase for Attendance
 */
class AttendanceTest extends \TYPO3\Flow\Tests\UnitTestCase
{

    /**
     * @test
     */
    public function makeSureThatSomethingHolds()
    {
        $this->markTestIncomplete('Automatically generated test case; you need to adjust this!');

        $expected = 'Foo';
        $actual = 'Foo'; // This should be the result of some function call
        $this->assertSame($expected, $actual);
    }
}
