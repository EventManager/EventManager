<?php
namespace KayStrobach\EventManager\Command;

/*                                                                        *
 * This script belongs to the Flow package "SingleSignOn".               *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Cli\CommandController;
use TYPO3\Party\Domain\Model\Person;
use TYPO3\Party\Domain\Model\PersonName;
use TYPO3\Flow\Security\Account;


/**
 * ImportExport command controller for the SingleSignOn package
 *
 * @Flow\Scope("singleton")
 */
class ImportExportCommandController extends CommandController {
	/**
	 * @var \TYPO3\Flow\Security\Cryptography\HashService
	 * @Flow\Inject
	 */
	protected $hashService;

	/**
	 * @Flow\Inject
	 * @var \TYPO3\Flow\Security\AccountRepository
	 */
	protected $accountRepository;

	/**
	 * @Flow\Inject
	 * @var \TYPO3\Flow\Security\AccountFactory
	 */
	protected $accountFactory;

	/**
	 * @Flow\Inject
	 * @var \TYPO3\Party\Domain\Repository\PartyRepository
	 */
	protected $partyRepository;

	/**
	 * @Flow\Inject
	 * @var \TYPO3\Flow\Persistence\PersistenceManagerInterface
	 */
	protected $persistenceManager;


    /**
	 * @param string $username
	 * @param string $password
	 * @param string $firstName
	 * @param string $lastName
	 * @param string $roles
	 * @param string $authenticationProvider
	 * @return bool
	 */
	public function userCommand($username, $password, $roles = "KayStrobach.EventManager:Administrator", $authenticationProvider = 'DefaultProvider') {
		$account = $this->accountRepository->findByAccountIdentifierAndAuthenticationProviderName($username, $authenticationProvider);
		if ($account instanceof Account) {
			$account->setCredentialsSource($this->hashService->hashPassword($password, 'default'));
			$this->outputFormatted('User: <em>' . $username . '</em> already existing ... now with password "' . $password . '".');
			$this->accountRepository->update($account);
			return FALSE;
		} else {
			$account = $this->accountFactory->createAccountWithPassword($username, $password, explode(',', $roles), $authenticationProvider);
			$this->accountRepository->add($account);

			$this->outputFormatted('User: <em>' . $username . '</em> created with password "' . $password . '".');
			$this->persistenceManager->persistAll();
		}
	}

	/**
	 * disables a user
	 *
	 *
	 * @param $userId
	 */
	public function disableUserCommand($userId) {

	}
	/**
	 * enables a user
	 *
	 *
	 * @param $userId
	 */
	public function enableUserCommand($userId) {

	}

}

?>