<?php
namespace KayStrobach\EventManager\Controller\Management;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use KayStrobach\EventManager\Domain\Model\Attendance;
use KayStrobach\EventManager\Domain\Model\Event;
use KayStrobach\EventManager\Domain\Repository\AttendanceRepository;
use TYPO3\Flow\Annotations as Flow;

class AttendeeController extends \TYPO3\Flow\Mvc\Controller\ActionController
{

    /**
     * @Flow\Inject
     * @var AttendanceRepository
     */
    protected $attendanceRepository;

    /**
     * @return void
     */
    public function indexAction(Event $event)
    {
        $this->view->assign('event', $event);
        $this->view->assign('attendees', $this->attendanceRepository->findByEvent($event));
    }

    /**
     * @param Attendance $attendance
     */
    public function removeAction(Attendance $attendance) {
        $this->attendanceRepository->remove($attendance);
        $this->redirect(
            'index',
            NULL,
            NULL,
            [
                'event' => $attendance->getEvent()
            ]
        );
    }

}
