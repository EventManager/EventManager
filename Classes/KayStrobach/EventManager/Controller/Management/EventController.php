<?php
namespace KayStrobach\EventManager\Controller\Management;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use Doctrine\ORM\Id\UuidGenerator;
use KayStrobach\EventManager\Domain\Model\Attendance;
use KayStrobach\EventManager\Domain\Model\Event;
use KayStrobach\EventManager\Domain\Model\Slot;
use KayStrobach\EventManager\Domain\Model\Workshop;
use KayStrobach\EventManager\Domain\Repository\AttendanceRepository;
use KayStrobach\EventManager\Domain\Repository\EventRepository;
use KayStrobach\EventManager\Domain\Repository\WorkshopRepository;
use KayStrobach\PhpOffice\View\ArrayExcelView;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Validation\Validator\UuidValidator;
use TYPO3\Media\Domain\Repository\AssetRepository;

class EventController extends \TYPO3\Flow\Mvc\Controller\ActionController
{
    /**
     * @var array
     */
    protected $viewFormatToObjectNameMap = array(
        'xlsx' => 'KayStrobach\\PhpOffice\\View\\ArrayExcelView'
    );

    /**
     * @Flow\Inject()
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * @Flow\Inject()
     * @var AttendanceRepository
     */
    protected $attendanceRepository;

    /**
     * @Flow\Inject()
     * @var WorkshopRepository
     */
    protected $workshopRepository;

    /**
     * @Flow\IgnoreValidation(argumentName="event")
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('events', $this->eventRepository->findAll());
    }

    /**
     * @Flow\IgnoreValidation(argumentName="event")
     * @param Event $event
     */
    public function newAction(Event $event = null) {
        $this->view->assign('event', new Event());
    }

    /**
     * @param Event $event
     */
    public function createAction(Event $event = null) {
        $this->eventRepository->add($event);
        $this->redirect(
            'edit',
            NULL,
            NULL,
            [
                'event' => $event
            ]
        );
    }

    /**
     * @Flow\IgnoreValidation(argumentName="event")
     * @param Event $event
     */
    public function editAction(Event $event) {
        $this->view->assign('event', $event);
        $this->view->assign('enableExport', 1);
        $this->view->assign('cloneableWorkshops', $this->workshopRepository->findbyEvent($event));
    }

    /**
     * @Flow\IgnoreValidation(argumentName="event")
     * @param Event $event
     */
    public function exportAction(Event $event) {
        if ($this->view instanceof ArrayExcelView) {
            $this->view->setTemplatePath('resource://KayStrobach.EventManager/Private/Templates/Management/Event/Export.xlsx');
        }

        $attendances = $this->attendanceRepository->findByEvent($event);

        $values = [
            [
                $event->getName()
            ],
            [
                $event->getStartDate()->format('d.m.Y H:i')
            ],
            [],
        ];

        /** @var Attendance $attendance */
        foreach($attendances as $attendance) {
            $values[] = $attendance->toArray();
        }

        $this->view->assign('values', $values);
    }

    /**
     * @param Event $event
     */
    public function updateAction(Event $event) {
        $this->eventRepository->update($event);
        $this->redirect(
            'edit',
            NULL,
            NULL,
            [
                'event' => $event
            ]
        );
    }

    /**
     * @param Event $event
     */
    public function addSlotAction(Event $event) {
        $slot = new Slot();
        $slot->setName('Slot name');
        $slot->setStart($event->getStartDate());
        $slot->setEnd($event->getEndDate());
        $event->addSlot($slot);
        $this->updateAction($event);
    }

    /**
     * @Flow\IgnoreValidation(argumentName="event")
     * @Flow\IgnoreValidation(argumentName="slot")
     * @param Event $event
     * @param Slot $slot
     */
    public function removeSlotAction(Event $event, Slot $slot) {
        $event->removeSlot($slot);
        $this->updateAction($event);
    }

    /**
     * @param Event $event
     * @param Slot $slot
     */
    public function addWorkshopAction(Event $event, Slot $slot) {
        $workshop = new Workshop();
        $workshop->setName('Workshop');
        $slot->addWorkshop($workshop);
        $this->eventRepository->update($event);
        $this->redirect(
            'edit',
            'Management\Workshop',
            NULL,
            [
                'workshop' => $workshop
            ]
        );
    }

    /**
     * @param Event $event
     * @param Slot $slot
     * @param Workshop $workshop
     */
    public function removeWorkshopAction(Event $event, Slot $slot, Workshop $workshop) {
        $slot->removeWorkshop($workshop);
        $this->updateAction($event);
    }

    /**
     * @param Event $event
     * @param Slot $targetSlot
     * @param Workshop $originalWorkshop
     */
    public function cloneWorkshopAction(Event $event, Slot $targetSlot, Workshop $originalWorkshop) {
        $targetSlot->addWorkshop(clone $originalWorkshop);
        $this->updateAction($event);
    }

}
