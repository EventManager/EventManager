<?php
namespace KayStrobach\EventManager\Controller\Management;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use KayStrobach\EventManager\Domain\Model\Workshop;
use KayStrobach\EventManager\Domain\Repository\EventRepository;
use KayStrobach\EventManager\Domain\Repository\WorkshopRepository;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Validation\Validator\UuidValidator;
use TYPO3\Media\Domain\Repository\AssetRepository;

class WorkshopController extends \TYPO3\Flow\Mvc\Controller\ActionController
{
    /**
     * @Flow\Inject()
     * @var WorkshopRepository
     */
    protected $workshopRepository;

    /**
     * @param Workshop $workshop
     */
    public function editAction(Workshop $workshop) {
        $this->view->assign('workshop', $workshop);
        $this->view->assign('event', $workshop->getSlot()->getEvent());
    }

    /**
     * @param Workshop $workshop
     */
    public function updateAction(Workshop $workshop) {
        $this->workshopRepository->update($workshop);
        $this->redirect(
            'edit',
            NULL,
            NULL,
            [
                'workshop' => $workshop
            ]
        );
    }

    /**
     * @param Workshop $workshop
     */
    public function removeAction(Workshop $workshop) {
        $this->workshopRepository->remove($workshop);
        $this->redirect(
            'edit',
            'Management\Event',
            NULL,
            [
                'event' => $workshop->getSlot()->getEvent()
            ]
        );
    }
}
