<?php
namespace KayStrobach\EventManager\Controller;

/*                                                                        *
 * This script belongs to the Flow package "SingleSignOn".               *
 *                                                                        *
 *                                                                        */

use KayStrobach\EventManager\Domain\Repository\EventRepository;
use TYPO3\Flow\Annotations as Flow;

/**
 * Standard controller for the SingleSignOn package
 *
 * @Flow\Scope("singleton")
 */
class StandardController extends \TYPO3\Flow\Mvc\Controller\ActionController {

    /**
     * @Flow\Inject()
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * Index action
     *
     * @return void
     */
    public function indexAction() {
        $events = $this->eventRepository->findAllVisible();
        if($events->count() === 1) {
            $this->forward(
                'show',
                'Event',
                NULL,
                [
                    'event' => $events->getFirst()
                ]
            );
        } else {
            $this->view->assign('events', $events);
        }
    }

}

?>