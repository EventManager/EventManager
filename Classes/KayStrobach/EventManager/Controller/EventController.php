<?php
namespace KayStrobach\EventManager\Controller;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use KayStrobach\Custom\Utility\MailUtility;
use KayStrobach\EventManager\Domain\DTO\Registration;
use KayStrobach\EventManager\Domain\Model\Attendance;
use KayStrobach\EventManager\Domain\Model\Event;
use KayStrobach\EventManager\Domain\Repository\AttendanceRepository;
use TYPO3\Flow\Annotations as Flow;

class EventController extends \TYPO3\Flow\Mvc\Controller\ActionController
{
    /**
     * @Flow\Inject()
     * @var AttendanceRepository
     */
    protected $attendanceRepository;

    /**
     * @Flow\Inject()
     * @var MailUtility
     */
    protected $mailUtility;

    /**
     * @Flow\IgnoreValidation(argumentName="event")
     * @return void
     */
    public function showAction(Event $event)
    {
        $this->view->assign('event', $event);
    }

    /**
     * @Flow\IgnoreValidation(argumentName="event")
     * @param Event $event
     */
    public function registerAction(Event $event) {
        if(!$event->isRegistrationOpen()) {
            $this->redirect(
                'show',
                null,
                null,
                array(
                    'event' => $event
                )
            );
        }
        $registration = new Registration();
        $registration->setEvent($event);
        $this->view->assign('registration', $registration);
        $this->view->assign('event', $registration->getEvent());
    }

    /**
     * @param Registration $registration
     */
    public function finishRegistrationAction(Registration $registration) {
        /** @todo avoid double registration */

        $attendance = new Attendance();
        $attendance->setEvent($registration->getEvent());
        $attendance->setPerson($registration->getPerson());
        $attendance->setWorkshops($registration->getSelectedWorkshops());

        $this->attendanceRepository->add($attendance);

        $this->mailUtility->send(
            $registration->getPerson()->getEmail(),
            'resource://KayStrobach.EventManager/Private/Mails/ThankYou.html',
            [
                'registration' => $registration,
                'emailTextIncludingReplacements' => $this->replacePlaceHoldersForMail($registration->getEvent()->getMailText(), $registration)
            ]
        );

        $this->redirect(
            'thankYou',
            NULL,
            NULL,
            [
                'event' => $registration->getEvent()
            ]
        );
    }

    public function thankYouAction(Event $event) {
        $this->view->assign('event', $event);
    }

    protected function replacePlaceHoldersForMail($text, Registration $registration) {
        $text = str_replace('###name###', $registration->getPerson()->getName(), $text);
        return $text;
    }
}
