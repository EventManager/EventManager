<?php
namespace KayStrobach\EventManager\Controller;

/*                                                                        *
 * This script belongs to the Flow package "SingleSignOn".               *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Authentication\Controller\AbstractAuthenticationController;
use TYPO3\Flow\Security\Exception\AuthenticationRequiredException;

/**
 * Standard controller for the SingleSignOn package
 *
 * @Flow\Scope("singleton")
 */
class AuthenticationController extends AbstractAuthenticationController
{
    /**
     * Index action
     *
     * @param string $username
     * @return void
     */
    public function indexAction($username = '')
    {
        $this->view->assign('username', $username);
        $this->view->assign('disableLoginButton', true);
    }

    /**
     * Is called if authentication was successful. If there has been an
     * intercepted request due to security restrictions, you might want to use
     * something like the following code to restart the originally intercepted
     * request:
     *
     * if ($originalRequest !== NULL) {
     *     $this->redirectToRequest($originalRequest);
     * }
     * $this->redirect('someDefaultActionAfterLogin');
     *
     * @param \TYPO3\Flow\Mvc\ActionRequest $originalRequest The request that was intercepted by the security framework, NULL if there was none
     * @return string
     */
    protected function onAuthenticationSuccess(\TYPO3\Flow\Mvc\ActionRequest $originalRequest = null)
    {
        if ($originalRequest !== null) {
            $this->redirectToRequest($originalRequest);
        } else {
            $this->redirect(
                'index',
                'Management\Event'
            );
        }
    }

    /**
     * Is called if authentication failed.
     *
     * Override this method in your login controller to take any
     * custom action for this event. Most likely you would want
     * to redirect to some action showing the login form again.
     *
     * @param AuthenticationRequiredException $exception The exception thrown while the authentication process
     * @return void
     */
    protected function onAuthenticationFailure(AuthenticationRequiredException $exception = null)
    {
        $this->flashMessageContainer->addMessage(
            new \TYPO3\Flow\Error\Error(
                'Leider wurde kein Nutzer zu diesen Nutzerdaten gefunden, bitte nochmal probieren!',
                ($exception === null ? 1347016771 : $exception->getCode())
            )
        );

        $arguments = $this->request->getInternalArguments();
        $username = \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($arguments, '__authentication.TYPO3.Flow.Security.Authentication.Token.UsernamePassword.username');

        $this->redirect(
            'index',
            null,
            null,
            array(
                'username' => $username
            )
        );
    }

    /**
     * Logs all active tokens out. Override this, if you want to
     * have some custom action here. You can always call the parent
     * method to do the actual logout.
     *
     * @return void
     */
    public function logoutAction()
    {
        parent::logoutAction();
        $this->redirect(
            'index'
        );
    }
}

?>