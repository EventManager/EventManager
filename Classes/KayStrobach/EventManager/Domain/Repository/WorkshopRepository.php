<?php
namespace KayStrobach\EventManager\Domain\Repository;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use KayStrobach\EventManager\Domain\Model\Event;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class WorkshopRepository extends Repository
{

    // add customized methods here

    public function findByEvent(Event $event)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('slot.event', $event)
        );
        $query->setDistinct();
        return $query->execute();
    }
}
