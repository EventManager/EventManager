<?php
namespace KayStrobach\EventManager\Domain\Repository;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class PersonRepository extends Repository
{

    // add customized methods here

}
