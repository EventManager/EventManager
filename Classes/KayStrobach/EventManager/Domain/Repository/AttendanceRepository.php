<?php
namespace KayStrobach\EventManager\Domain\Repository;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use KayStrobach\EventManager\Domain\Model\Event;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class AttendanceRepository extends Repository
{

    /**
     * @param Event $event
     * @return \TYPO3\Flow\Persistence\QueryResultInterface
     */
    public function findByEvent(Event $event) {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('event', $event)
        );
        return $query->execute();
    }
}
