<?php
namespace KayStrobach\EventManager\Domain\Model;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use TYPO3\Media\Domain\Model\Image;

/**
 * @Flow\Entity
 */
class Workshop
{
    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="StringLength", options={ "minimum"=3, "maximum"=255 })
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var Image
     * @ORM\OneToOne(cascade={"all"})
     */
    protected $coverImage;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $startDate = null;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $endDate = null;

    /**
     * @var integer
     * @ORM\Column(nullable=true)
     */
    protected $places = null;

    /**
     * @var \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Person>
     * @ORM\ManyToMany(cascade={"persist"}))
     */
    protected $speakers;

    /**
     * @var \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Attendance>
     * @ORM\ManyToMany(cascade={"persist"}, mappedBy="workshops"))
     */
    protected $attendances;

    /**
     * @var Slot
     * @ORM\ManyToOne(inversedBy="workshops", cascade={"persist"})
     */
    protected $slot;

    /**
     * @ORM\Column(nullable=true)
     * @var integer
     */
    protected $sorting;

    public function __construct()
    {
        $this->attendances = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Image
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * @param Image $coverImage
     */
    public function setCoverImage($coverImage)
    {
        $this->coverImage = $coverImage;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return int
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @param int $places
     */
    public function setPlaces($places)
    {
        $this->places = $places;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpeakers()
    {
        return $this->speakers;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $speakers
     */
    public function setSpeakers($speakers)
    {
        $this->speakers = $speakers;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttendances()
    {
        return $this->attendances;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $attendances
     */
    public function setAttendances($attendances)
    {
        $this->attendances = $attendances;
    }

    public function getAttendancesPercent() {
        if (($this->getPlaces() === 0) || ($this->getPlaces() === null)) {
            return null;
        }
        return round($this->attendances->count() / $this->getPlaces() * 100);
    }

    /**
     * @return Slot
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param Slot $slot
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }
}
