<?php
namespace KayStrobach\EventManager\Domain\Model;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use TYPO3\Party\Domain\Model\PersonName;

/**
 * @Flow\Entity
 */
class Person
{
    /**
     * @Flow\Validate(type="NotEmpty")
     * @ORM\OneToOne
     * @var PersonName
     */
    protected $name;

    /**
     * @var string
     */
    protected $organisation;

    /**
     * @ORM\Column(nullable=true)
     * @var string
     */
    protected $organisationType;

    /**
     * @var string
     */
    protected $organisationOffice;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="emailAddress")
     * @var string
     */
    protected $email;

    /**
     * @ORM\ManyToOne(cascade={"all"})
     * @Flow\Validate(type="NotEmpty", validationGroups={"passbild"})
     * @Flow\Validate(type="KayStrobach.Custom:File\MimeType")
     * @var \TYPO3\Flow\Resource\Resource
     */
    protected $image;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    protected $vita;

    /**
     * @var \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Attendance>
     * @ORM\OneToMany(cascade={"persist"}, mappedBy="person"))
     */
    protected $attendances;

    /**
     * @return PersonName
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param PersonName $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * @param string $organisation
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     * @return string
     */
    public function getOrganisationType()
    {
        return $this->organisationType;
    }

    /**
     * @param string $organisationType
     */
    public function setOrganisationType($organisationType)
    {
        $this->organisationType = $organisationType;
    }

    /**
     * @return string
     */
    public function getOrganisationOffice()
    {
        return $this->organisationOffice;
    }

    /**
     * @param string $organisationOffice
     */
    public function setOrganisationOffice($organisationOffice)
    {
        $this->organisationOffice = $organisationOffice;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return \TYPO3\Flow\Resource\Resource
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \TYPO3\Flow\Resource\Resource $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getVita()
    {
        return $this->vita;
    }

    /**
     * @param string $vita
     */
    public function setVita($vita)
    {
        $this->vita = $vita;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttendances()
    {
        return $this->attendances;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $attendances
     */
    public function setAttendances($attendances)
    {
        $this->attendances = $attendances;
    }
}
