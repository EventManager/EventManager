<?php
namespace KayStrobach\EventManager\Domain\Model;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Attendance
{
    /**
     * @var \KayStrobach\EventManager\Domain\Model\Person
     * @ORM\ManyToOne(cascade={"persist"}, inversedBy="attendances"))
     */
    protected $person;

    /**
     * @var \KayStrobach\EventManager\Domain\Model\Event
     * @ORM\ManyToOne(cascade={"persist"}, inversedBy="attendances"))
     */
    protected $event;

    /**
     * @var \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Workshop>
     * @ORM\ManyToMany(cascade={"persist"}, inversedBy="attendances"))
     * @ORM\OrderBy({"startDate" = "ASC"})
     */
    protected $workshops;

    public function __construct()
    {
        $this->workshops = new ArrayCollection();
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param Person $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Workshop>
     */
    public function getWorkshops()
    {
        return $this->workshops;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Workshop> $workshops
     */
    public function setWorkshops(\Doctrine\Common\Collections\Collection $workshops = null)
    {
        if ($workshops === null) {
            $this->workshops = new ArrayCollection();
            return;
        }
        foreach($workshops as $workshop) {
            if ($workshop === null) {
                $workshops->removeElement($workshop);
            }
        }
        $this->workshops = $workshops;
    }

    /**
     *
     */
    public function toArray() {
        $array = [
            'title' => $this->person->getName()->getTitle(),
            'firstName' => $this->person->getName()->getFirstName(),
            'lastName' => $this->person->getName()->getLastName(),
            'organisation' => $this->person->getOrganisation(),
            'buero' => $this->person->getOrganisationOffice(),
            'email' => $this->person->getEmail(),
        ];

        /** @var Workshop $workshop */
        foreach ($this->getWorkshops() as $workshop) {
            $array[] = $workshop->getName();
        }
        return $array;
    }
}
