<?php
namespace KayStrobach\EventManager\Domain\Model;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use TYPO3\Media\Domain\Model\Document;
use TYPO3\Media\Domain\Model\Image;

/**
 * @Flow\Entity
 */
class Event
{
    /**
     * @var \TYPO3\Flow\Persistence\PersistenceManagerInterface
     * @Flow\Transient()
     * @Flow\Inject()
     */
    protected $persistenceManager;

    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="StringLength", options={ "minimum"=3, "maximum"=255 })
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $teaser;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $footerText;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="StringLength", options={ "minimum"=3, "maximum"=255 })
     * @Flow\Validate(type="Alphanumeric")
     */
    protected $slug;

    /**
     * @var Image
     * @ORM\OneToOne(cascade={"all"})
     */
    protected $logoImage;

    /**
     * @var Image
     * @ORM\OneToOne(cascade={"all"})
     */
    protected $coverImage;

    /**
     * @var Document
     * @ORM\OneToOne(cascade={"all"})
     */
    protected $downloadFile;

    /**
     * @var Image
     * @ORM\OneToOne(cascade={"all"})
     */
    protected $logo1;

    /**
     * @var Image
     * @ORM\OneToOne(cascade={"all"})
     */
    protected $logo2;

    /**
     * @var Image
     * @ORM\OneToOne(cascade={"all"})
     */
    protected $logo3;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $startDate = null;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $endDate = null;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $registrationStartDate = null;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $registrationEndDate = null;

    /**
     * @var \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Attendance>
     * @ORM\OneToMany(cascade={"persist"}, mappedBy="event"))
     */
    protected $attendances;

    /**
     * @var \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Slot>
     * @ORM\OneToMany(cascade={"all"}, mappedBy="event"))
     * @ORM\OrderBy({"start" = "ASC"})
     */
    protected $slots;

    /**
     * @var string
     * @Flow\Validate(type="StringLength", options={ "minimum"=3, "maximum"=255 })
     * @ORM\Column(nullable=true)
     */
    protected $mailSubject;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $mailText;

    /**
     * @var string
     */
    protected $slotRendering = null;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * @param string $teaser
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    /**
     * @return string
     */
    public function getFooterText()
    {
        return $this->footerText;
    }

    /**
     * @param string $footerText
     */
    public function setFooterText($footerText)
    {
        $this->footerText = $footerText;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Image
     */
    public function getLogoImage()
    {
        return $this->logoImage;
    }

    /**
     * @param Image $logoImage
     */
    public function setLogoImage($logoImage = null)
    {
        $this->logoImage = $logoImage;
    }

    /**
     * @return Image
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * @param Image $coverImage
     */
    public function setCoverImage($coverImage = null)
    {
        $this->coverImage = $coverImage;
    }

    /**
     * @return mixed
     */
    public function getDownloadFile()
    {
        return $this->downloadFile;
    }

    /**
     * @param mixed $downloadFile
     */
    public function setDownloadFile($downloadFile)
    {
        $this->downloadFile = $downloadFile;
    }

    /**
     * @return Image
     */
    public function getLogo1()
    {
        return $this->logo1;
    }

    /**
     * @param Image $logo1
     */
    public function setLogo1($logo1 = null)
    {
        $this->logo1 = $logo1;
    }

    /**
     * @return Image
     */
    public function getLogo2()
    {
        return $this->logo2;
    }

    /**
     * @param Image $logo2
     */
    public function setLogo2($logo2 = null)
    {
        $this->logo2 = $logo2;
    }

    /**
     * @return Image
     */
    public function getLogo3()
    {
        return $this->logo3;
    }

    /**
     * @param Image $logo3
     */
    public function setLogo3($logo3)
    {
        $this->logo3 = $logo3;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getRegistrationStartDate()
    {
        return $this->registrationStartDate;
    }

    /**
     * @param mixed $registrationStartDate
     */
    public function setRegistrationStartDate($registrationStartDate)
    {
        $this->registrationStartDate = $registrationStartDate;
    }

    /**
     * @return mixed
     */
    public function getRegistrationEndDate()
    {
        return $this->registrationEndDate;
    }

    /**
     * @param mixed $registrationEndDate
     */
    public function setRegistrationEndDate($registrationEndDate)
    {
        $this->registrationEndDate = $registrationEndDate;
    }

    /**
     * @param Slot $slot
     */
    public function addSlot(Slot $slot) {
        if($this->slots === null) {
            $this->slots = new \Doctrine\Common\Collections\ArrayCollection();
        }
        if(!$this->slots->contains($slot)) {
            $slot->setEvent($this);
            $this->slots->add($slot);
        }
    }

    /**
     * @param Slot $slot
     */
    public function removeSlot(Slot $slot) {
        if($this->slots === null) {
            $this->slots = new \Doctrine\Common\Collections\ArrayCollection();
        }
        $this->slots->removeElement($slot);
        $this->persistenceManager->remove($slot);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlots()
    {
        return $this->slots;
    }

    public function getSlotsWithWorkshop() {
        $slots = new ArrayCollection();
        /** @var Slot $slot */
        foreach($this->slots as $slot) {
            if(($slot->getWorkshops()) && ($slot->getWorkshops()->count() > 0)) {
                $slots->add($slot);
            }
        }
        return $slots;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $slots
     */
    public function setSlots($slots)
    {
        $this->slots = $slots;
    }

    /**
     * @return string
     */
    public function getSlotRendering()
    {
        return $this->slotRendering;
    }

    /**
     * @param string $slotRendering
     */
    public function setSlotRendering($slotRendering)
    {
        $this->slotRendering = $slotRendering;
    }

    public function getSlotsRenderings() {
        return [
            'page-blocks' => 'Big blocks',
            'page-blocks-small' => 'Small Blocks',
            'timeline' => 'Timeline',
        ];
    }

    /**
     * @return string
     */
    public function getMailSubject()
    {
        return $this->mailSubject;
    }

    /**
     * @param string $mailSubject
     */
    public function setMailSubject($mailSubject)
    {
        $this->mailSubject = $mailSubject;
    }

    /**
     * @return string
     */
    public function getMailText()
    {
        return $this->mailText;
    }

    /**
     * @param string $mailText
     */
    public function setMailText($mailText)
    {
        $this->mailText = $mailText;
    }

    public function isRegistrationOpen() {
        $now = new \DateTime('now');
        if($now < $this->registrationStartDate) {
            return false;
        }
        if($now > $this->registrationEndDate) {
            return false;
        }
        return true;
    }
}
