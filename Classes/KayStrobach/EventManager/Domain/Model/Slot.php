<?php
namespace KayStrobach\EventManager\Domain\Model;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Slot
{
    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $start;

    /**
     * @var \DateTime
     * @ORM\Column(nullable=true)
     */
    protected $end;

    /**
     * @var \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Workshop>
     * @ORM\OneToMany(cascade={"all"}, mappedBy="slot"))
     * @ORM\OrderBy({"sorting" = "ASC", "startDate" = "ASC"})
     */
    protected $workshops;

    /**
     * @var \KayStrobach\EventManager\Domain\Model\Event
     * @ORM\ManyToOne(cascade={"persist"}, inversedBy="slots"))
     */
    protected $event;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param mixed $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param mixed $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }
    /**
     * @param Workshop $workshop
     */
    public function addWorkshop(Workshop $workshop) {
        if($this->workshops === null) {
            $this->workshops = new \Doctrine\Common\Collections\ArrayCollection();
        }
        $workshop->setSlot($this);
        $workshop->setStartDate($this->getStart());
        $workshop->setEndDate($this->getEnd());
        if(!$this->workshops->contains($workshop)) {
            $this->workshops->add($workshop);
        }
    }

    /**
     * @param Workshop $workshop
     */
    public function removeWorkshop(Workshop $workshop) {
        if($this->workshops === null) {
            $this->workshops = new \Doctrine\Common\Collections\ArrayCollection();
        }
        $this->workshops->removeElement($workshop);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkshops()
    {
        if($this->workshops === null) {
            $this->workshops = new \Doctrine\Common\Collections\ArrayCollection();
        }
        return $this->workshops;
    }

    public function getWorkshopsWithFreeSeats() {
        $workshops = new ArrayCollection();
        /** @var Workshop $workshop */
        foreach ($this->workshops as $workshop) {
            if (($workshop->getPlaces() === null) || ($workshop->getPlaces() === 0)) {
                $workshops->add($workshop);
            } elseif ($workshop->getPlaces() >= 1) {
                if ($workshop->getAttendances()->count() < $workshop->getPlaces()) {
                    $workshops->add($workshop);
                }
            }
        }
        return $workshops;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $workshops
     */
    public function setWorkshops($workshops)
    {
        $this->workshops = $workshops;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }
}
