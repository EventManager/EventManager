<?php
namespace KayStrobach\EventManager\Domain\DTO;

/*
 * This file is part of the KayStrobach.EventManager package.
 */

use KayStrobach\EventManager\Domain\Model\Event;
use KayStrobach\EventManager\Domain\Model\Person;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;



class Registration
{
    /**
     * @var Person
     */
    protected $person;

    /**
     * @var Event
     */
    protected $event;

    /**
     * @ORM\ManyToMany(cascade={"persist"})
     * @var \Doctrine\Common\Collections\ArrayCollection<\KayStrobach\EventManager\Domain\Model\Workshop>
     */
    protected $selectedWorkshops;

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param Person $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Workshop>
     */
    public function getSelectedWorkshops()
    {
        return $this->selectedWorkshops;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection<\KayStrobach\EventManager\Domain\Model\Workshop> $selectedWorkshops
     */
    public function setSelectedWorkshops($selectedWorkshops)
    {
        $this->selectedWorkshops = $selectedWorkshops;
    }
}